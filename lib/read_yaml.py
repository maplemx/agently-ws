import yaml

def read_yaml(yaml_path:str):
    try:
        with open(yaml_path, "r") as yaml_file:
            yaml_dict = yaml.safe_load(yaml_file)
            return yaml_dict
    except Exception as e:
        raise Exception(f"Error occured when read YAML from path '{ yaml_path }'.\nError: { str(e) }")