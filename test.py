import asyncio
import aiohttp
from aiohttp_socks import ProxyConnector
import json

# 测试接口chat数据
# 第一次：
"""
request_data = {
    "event": "chat",
    "request_id": 1234,
    "data": {
        "user_input": "今天天气不错，记得提醒我待会要去买1盒牛奶，3个鸡蛋",
        "session_id": "todo",
    }
}
"""
#第二次：
"""
request_data = {
    "event": "chat",
    "request_id": 1234,
    "data": {
        "user_input": "我们刚才说了什么？",
        "session_id": "todo",
    }
}
"""
async def proxy_request():
    async with aiohttp.ClientSession() as session:
        async with session.ws_connect('ws://127.0.0.1:15365/') as websocket:
            await websocket.send_str(json.dumps(request_data))
            # 接收代理响应
            async for msg in websocket:
                response = msg.data
                print(json.loads(response))

asyncio.get_event_loop().run_until_complete(proxy_request())