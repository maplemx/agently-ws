required_params = ["user_input"]

def handler(data: dict, agent_factory: object, write_message: callable):
    agent_id = data["agent_id"] if "agent_id" in data else "demo_agent"
    session_id = data["session_id"] if "session_id" in data else "demo_session"
    agent = agent_factory.create_agent(agent_id)
    agent.active_session(session_id)
    #streaming
    agent.on_delta(lambda data: write_message({ "type": "delta", "content": data }))
    reply = agent.input(data["user_input"]).start()
    agent.stop_session()
    return { "type": "complete", "content": reply }